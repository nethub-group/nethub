import os
import shutil
import subprocess
#
from django.db import models
from django.contrib.auth.models import User
# from django.core.exceptions import ValidationError
#
from app.settings import GIT_ADDRESS, AUTH_USER_MODEL


class Basemodel(models.Model):
    """Base class containing all data models common information"""
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)
    order = models.IntegerField(default=0)

    class Meta:
        # Define Model as abstract
        abstract = True


class Project(Basemodel):
    """Neuro net project data model"""
    title = models.CharField(
        max_length=100,
        null=False,
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=False,
    )
    library = models.CharField(
        max_length=100,
        null=False,
    )
    git_address = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )

    def __str__(self):
        return '%s/%s' % (self.user, self.title)

    # def clean(self):

    def save(self, *args, **kwargs):
        """Overriding save model method"""
        if not self.pk:
            # This code only happens if the object is not in the database yet.
            # Otherwise it would have pk

            # Check if this project title already exists for given user
            check = Project.objects.filter(title=self.title, user=self.user)
            if check.count() > 0:
                raise Exception('Project with this name already exist')

            # Check if repository already exists at the git server
            repo_path = '/git/%s/%s.git' % (self.user.username, self.title)
            if os.path.exists(repo_path):
                raise Exception('Repo with this name already exist')


            # Create user directory at the git server if it doesn't exists
            user_path = '/git/%s' % (self.user.username)
            if not os.path.exists(user_path):
                os.makedirs(user_path)

            # Initialize git repository
            repo_path = '/git/%s/%s.git' % (self.user.username, self.title)
            command = 'git init --bare %s' % (repo_path)
            subprocess.call(command, shell=True)

        # Redefine git project url
        self.git_address = '%s/%s/%s.git' % (
            GIT_ADDRESS,
            self.user.username,
            self.title,
        )

        # Call the "real" save() method
        super(Project, self).save(*args, **kwargs)

    # Overriding delete model method
    def delete(self, *args, **kwargs):
        # Remove folder in git server structure
        repo_path = '/git/%s/%s.git' % (self.user, self.title)
        if os.path.exists(repo_path):
            shutil.rmtree(repo_path)
        # Call the "real" delete() method
        super(Project, self).delete(*args, **kwargs)


class Slide(Basemodel):
    """Slide model, for the frontend"""
    header = models.CharField(max_length=500)
    image = models.ImageField(
        upload_to='slides/',
        default='default.png',
        )

    def __str__(self):
        return 'slide id %s' % (self.pk)
