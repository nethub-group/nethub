import graphene
from graphene import relay
from graphene_django import DjangoObjectType
import django_filters
from graphene_django.filter import DjangoFilterConnectionField
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth import authenticate


class UserNode(DjangoObjectType):
    class Meta:
        model = get_user_model()
        filter_fields = {
            'username': ['exact'],
        }
        interfaces = (relay.Node, )


class UserFilter(django_filters.FilterSet):
    class Meta:
        model = get_user_model()
        fields = ['username']


class CreateUser(graphene.Mutation):
    user = graphene.Field(UserNode)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, username, password, email):
        user = get_user_model()(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()

        return CreateUser(user=user)


class LogIn(graphene.Mutation):
    user = graphene.Field(UserNode)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    def mutate(self, info, username, password):
        user = authenticate(username=username, password=password)
        # print('JUST AUTHEN', user)

        if not user:
            raise Exception('Invalid username or password!')

        # info.context.session['token'] = user.token
        return LogIn(user=user)


class Mutation(graphene.ObjectType):
    createUser = CreateUser.Field()
    login = LogIn.Field()


class Query(graphene.AbstractType):
    me = graphene.Field(UserNode)
    users = graphene.List(UserNode)
    user = DjangoFilterConnectionField(UserNode,
                                      filterset_class=UserFilter)

    def resolve_users(self, info):
        return User.objects.all()

    def resolve_me(self, info):
        # print('CONTEXT USER', info.context.user)
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        return user
