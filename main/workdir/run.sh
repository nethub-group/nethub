#!/bin/bash
if [[ $1 = "help" ]]; then
echo "  "
echo "###################################################################"
echo "#                                                                 #"
echo "# init    - Running init process                                  #"
echo "# web     - Running Django dev server                             #"
echo "# uwsgi   - Running uwsgi server                                  #"
echo "# migrate - makemigratea; migrate; schema                         #"
echo "# schema  - graphql_schema --indent 2                             #"
echo "# flower  - celery -A app flower                                  #"
echo "# jupyter - jupyter notebook --ip 0.0.0.0                         #"
echo "#                                                                 #"
echo "###################################################################"
echo "#                                                                 #"
echo "# cbeat   - celery -A app beat                                    #"
echo "# celery  - celery -A app worker --loglevel=info                  #"
echo "# cback   - celery -A app worker --loglevel=info -Q back -n back  #"
echo "# cpurge  - celery -A app purge -f;celery -A app purge -Q back -f #"
echo "#                                                                 #"
echo "###################################################################"
echo "  "
exit 1
fi

if [[ $1 = "init" ]]; then
  echo "Running init process"
  git config --global user.email "nethub@yandex.ru"
  git config --global user.name "nethub"
  ./run.sh migrate
  exit 1
fi

if [[ $1 = "web" ]]; then
  echo "Running Django develop web-server"
  pkill -f runserver
  python manage.py runserver 0.0.0.0:8000
  exit 1
fi

if [[ $1 = "uwsgi" ]]; then
  echo "Run uwsgi server"
  uwsgi uwsgi.ini
  exit 1
fi

if [[ $1 = "migrate" ]]; then
  python manage.py makemigrations
  python manage.py makemigrations ncore

  python manage.py migrate --database=default

  python manage.py graphql_schema --indent 2
  mv schema.json /data/.

  exit 1
fi

if [[ $1 = "schema" ]]; then
  python manage.py graphql_schema --indent 2
  mv schema.json /data/.
  exit 1
fi

if [[ $1 = "flower" ]]; then
  celery -A app flower
  exit 1
fi

if [[ $1 = "jupyter" ]]; then
  jupyter notebook --ip 0.0.0.0 --allow-root
  exit 1
fi
################################################################

if [[ $1 = "cbeat" ]]; then
  clear
  celery -A app beat
  exit 1
fi

if [[ $1 = "celery" ]]; then
  clear
  celery -A app worker --loglevel=info
  exit 1
fi

if [[ $1 = "cback" ]]; then
  clear
  celery -A app worker --loglevel=info -Q back -n back
  exit 1
fi

if [[ $1 = "cpurge" ]]; then
  clear
  celery -A app purge -f
  celery -A app purge -Q back -f
  exit 1
fi

# DEFAULT COMMANDS WITHOUT ARGUMENTS
./run.sh web
