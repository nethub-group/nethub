#!/usr/bin/python
import os
import sys
#
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
try:
    import django
except ImportError as exc:
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    ) from exc
print('Setup django environment...')
# Setup Django env
django.setup()
#
from app.getagrids import getaGrids
from processor.processbyday import processByDay
from processor.processbyset import processBySet
from processor.extractfromset import extractFromSet
from processor.processfileswithinsert import processFilesWithInsert
#
# MAIN
if __name__ == "__main__":
    """ Processing command line arguments """
    if len(sys.argv) > 1:
        if 'byday' in sys.argv:
            processByDay.delay()
        if 'byset' in sys.argv:
            processBySet.delay()
        if 'insert' in sys.argv:
            processFilesWithInsert()
        if 'extractset' in sys.argv:
            extractFromSet.delay()
        if 'getagrids' in sys.argv:
            getaGrids()
    else:
        """ No arguments """
        pass
