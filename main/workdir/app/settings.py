import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$2e+2i!v3dtOr@ne8w8=!r2gd4@vo43ny2t0x($2)zu11l5gb0'

# SECURITY WARNING: don't run with debug turned on in production!
# Setting DEBUG flag in depence from environment variable
DEBUG = False
TESTMODE = False


ALLOWED_HOSTS = [
    '127.0.0.1',
    '0.0.0.0',
]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'rest_framework',

    # A Django App that adds Cross-Origin Resource Sharing headers to responses
    'corsheaders',
    # GraphQL server implementation framework
    'graphene_django',
    # 'imagekit',

    # Own applications
    'users',  # users app
    'ncore',  # core app
    # 'beat',  # celery periodic tasks scheduler
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'corsheaders.middleware.CorsMiddleware',  # Needed for django-cors-headers
]

AUTHENTICATION_BACKENDS = [
    'graphql_jwt.backends.JSONWebTokenBackend',  # JWT authentication
    'django.contrib.auth.backends.ModelBackend',
]

"""
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
}
"""

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'maindb',
        'USER': 'postgres',
        'HOST': 'main_maindb',
        'PORT': 5432,
        'PASSWORD': 'Passw0rd33',
    },
}
"""

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static/")
URL_STATIC_ROOT = os.path.join(BASE_DIR, 'static')


# Media settings
MEDIA_URL = '/media/'
MEDIA_ROOT = 'media'
URL_MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


# Admin localization settings
# ADMIN_SITE_HEADER = "GraphQL Server"
# ADMIN_SITE_TITLE = "GraphQL Server"
# ADMIN_INDEX_TITLE = "GraphQL Server"


# CORS access settings
CORS_ORIGIN_WHITELIST = (
    '127.0.0.1:3000',
    '0.0.0.0:3000',
    '127.0.0.1:21021',
    '0.0.0.0:21021',
)

# Graphene schem file location
GRAPHENE = {
    'SCHEMA': 'app.schema.schema',
    'MIDDLEWARE': [
        'graphql_jwt.middleware.JSONWebTokenMiddleware',  # JWT authentication
    ],
}

# Our custom user model
AUTH_USER_MODEL = 'users.User'

# Celery config
CELERY_BROKER_URL = 'redis://nethub_main_redis'
CELERY_RESULT_BACKEND = 'redis://nethub_main_redis'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'
# Celery beat time rate for checking new data files
BEAT_RATE = 5.0

#Static for NGINX
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

# Git config
GIT_ADDRESS = 'http://127.0.0.1:21026'

# Try to import local settings from local_settings.py
try:
    from app.local_settings import *
except ImportError:
    pass
