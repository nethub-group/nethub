from __future__ import absolute_import, unicode_literals
import logging
import os
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celeryApp
#
#
# logging.basicConfig(level=logging.DEBUG)

__all__ = ['celeryApp']

# Create directories structure for parseAndInsert task
