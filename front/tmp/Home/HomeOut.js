import React, { Component } from 'react'
import styled from 'styled-components'
//
import { media } from '../../constants'
import Sliders from './Sliders'


const StyledHomeOut = styled.div`
  .carousel .carousel-item {
    border: dashed 2px greeen;
  }

  ${media.tablet`
  `}
`

export default class Home extends Component {
  render() {
    return (
      <StyledHomeOut>
        <Sliders />
      </StyledHomeOut>
    );
  }
}
