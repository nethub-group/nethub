import React, { Component } from 'react'
import styled from 'styled-components'
// import SvgIcon from 'react-icons-kit'
import { single } from 'react-icons-kit/entypo/single'
import { database } from 'react-icons-kit/icomoon/database'
// import { users } from 'react-icons-kit/icomoon/users'
import { location2 } from 'react-icons-kit/icomoon/location2'
import { podcast } from 'react-icons-kit/icomoon/podcast'
// import {happy} from 'react-icons-kit/icomoon/happy'
import { book } from 'react-icons-kit/icomoon/book'
import {user} from 'react-icons-kit/fa/user'
//
import { media } from '../../../constants'
// import { USER_ID,  USER_NAME } from '../../constants'
import ServicesMenu from './ServicesMenu'
import MenuLink from './MenuLink'
// import Auth from './Auth'
/* End of import section ************************************** */

const StyledMenu = styled.div`
  width: 300px;
  padding: 1em 0 1em 0;
  .item {
    display: block;
    padding: 0.2em 0.6em 0.2em 2.8em;
    color: white;
    text-decoration: none;
    line-height: 22px;
    cursor: pointer;
    user-select: none;
    font-size: 0.9em;
  }
  .icon {
    float: left;
    margin: 2px 10px 0 -2em;
  }
  .item:hover {
    background-color: #459345;
  }
  .item .active {
    background-color: #459345;
  }
  .user {
    color: white;
    padding: 0 1em 0 2.6em;
    font-weight: bold;
    text-transform: uppercase;
  }
  .more {
    float: right;
    padding-right: 0px;
  }

  ${media.tablet`
    width: 100%;
  `}
`

export default class Menu extends Component {
  render () {
    // const userId = localStorage.getItem(USER_ID)
    const userId = false
    const userName = 'tester'
    // const userName = localStorage.getItem(USER_NAME)

    return (
      <StyledMenu>
        <MenuLink
          link="/ceny"
          icon={database}
          title="Цены"
          _handleClick={this.props._handleClick}
        />
        <MenuLink
          link="/specialisty"
          icon={user}
          title="Сотрудники"
          _handleClick={this.props._handleClick}
        />
        <ServicesMenu
          menuData={this.props.menuData}
          _handleClick={this.props._handleClick}
        />
        {/*
        <MenuLink
          link={'/specialisty'}
          icon={users}
          title={'Сотрудники'}
          _handleClick={this.props._handleClick}
        />
        <MenuLink
          link={'/portfolio'}
          icon={happy}
          title={'Наши работы'}
          _handleClick={this.props._handleClick}
        />
        */}
        <MenuLink
          link="/informaciya/licenzii-nashih-klinik"
          icon={book}
          title="Лицензии"
          _handleClick={this.props._handleClick}
        />
        <MenuLink
          link="/otzyvy"
          icon={podcast}
          title="Отзывы"
          _handleClick={this.props._handleClick}
        />
        <MenuLink
          link="/adresa-stomatologicheskih-klinik-satori-dent"
          icon={location2}
          title="Контакты"
          _handleClick={this.props._handleClick}
        />
        {userId &&
        <div>
          <hr />
          <div className="user">{userName}</div>
          <MenuLink
            link="/services/create"
            icon={single}
            title="Добавить услугу"
            _handleClick={this.props._handleClick}
          />
        </div>
        }
      </StyledMenu>
    )
  }
}
// <Auth history={this.props.history} />
