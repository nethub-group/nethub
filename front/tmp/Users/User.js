import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import styled from 'styled-components'
import { Table } from 'reactstrap'
import { Link } from 'react-router-dom'
//
import environment from '../../Environment'
import { media } from '../../constants'


const StyledUser = styled.div`
  .info {
    background-color: #bbb;
    padding: 1em 4em .6em 4em;
    .user {
      font-size: 34px;
      font-weight: bold;
      .icon {
        padding: 2px 20px;
        background-color: #eee;
        border-radius: 2em;
        text-transform: uppercase;
      }
      .name {
        padding: 0 0 0 .5em;
      }
    }
    .menu {
      padding: .4em 0 0 0;
      font-size: 1.4em;
    }
  }
  .content {
    padding: 0 2em;
  }

  ${media.tablet`
  `}
`


const query = graphql`
  query UserQuery($username: String!) {
    user(username: $username) {
      edges {
        node {
          id
          username
          projectSet {
            edges {
              node {
                id
                title
                library
              }
            }
          }
        }
      }
    }
  }
`


export default class User extends Component {
  render() {
    const username = this.props.match.params.username

    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{username: username}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('TOP props', props);

          const edges = props.user.edges[0].node.projectSet.edges
          const nets = edges.map(({node}, key) =>(
            <NetsRow
              key={key}
              counter={key}
              title={node.title}
              library={node.library}
              username={username}
              />
          ))

          return <StyledUser>
            <div className='info'>
              <div className='user'>
                <span className='icon'>
                  {username[0]}
                </span>
                <span className='name'>
                  {username}
                </span>
              </div>
              <div className='menu'>
                Neuronets
              </div>
            </div>
            <div className='content'>
              <Table>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Library</th>
                  </tr>
                </thead>
                <tbody>
                  {nets}
                </tbody>
              </Table>
            </div>
          </StyledUser>
        }
        return <div></div>
      }}
    />
  }
}


const NetsRow = ({counter, title, username, library}) =>
  <tr>
    <th scope="row">{++counter}</th>
    <td>
      <Link to={'/users/' + username + '/' + title}>{title}</Link>
    </td>
    <td>{library}</td>
  </tr>
