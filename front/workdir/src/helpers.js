export function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export function arraySortByOrder(arr) {
  let a = arr.map(x => x)
  let i = a.length - 1
  for (i; i>=0; i--) {
    for (let j=0; j<i; j++) {
      if (a[j].order > a[j+1].order) {
        let tmp = a[j]
        a[j] = a[j+1]
        a[j+1] = tmp
      }
    }
  }
  let normalized = a.map((item, key) => {
    let copy = Object.assign({}, item)
    copy.order = key
    return copy
  })
  return normalized
}

export function arrayMoveByOrder(arr, oldIndex, newIndex) {
  let removed = arr.splice(oldIndex, 1)
  arr.splice(newIndex, 0, removed[0])

  let normalized = arr.map((item, key) => {
    let copy = Object.assign({}, item)
    copy.order = key
    return copy
  })
  return normalized
}

// Create array with children id from array with parent id
export function makeChildren(arr) {
  // Plaining array, adding 'root' as parentId to the to top elements
  let plain = arr.map(({node}) => {
    return {
      id: node.id,
      parentId: node.parent?node.parent.id:'root',
      title: node.title,
    }
  })

  let childrenTree = []  // Menu items with children
  const getChildren = (id) => {
    let children = []
    plain.forEach(el => {
      if (el.parentId === id) {
        children = [...children, el.id]
      }
    })
    return children
  }

  let children = getChildren('root')
  childrenTree.push(
    {
      id: 'root',
      title: 'Top menu level',
      children: children,
    }
  )

  plain.forEach(item => {
    let children = getChildren(item.id)
    childrenTree.push(
      {
        id: item.id,
        title: item.title,
        children: children,
      }
    )
  })
}

export function makeTreeFromParent(arr) {
  // Plaining array, adding 'root' as parentId to the to top elements
  let plain = arr.map(({node}) => {
    return {
      id: node.id,
      parentId: node.parent?node.parent.id:'root',
      title: node.title,
      link: node.link,
      order: node.order,
    }
  })

  function getChildren(parentId) {
    let children = []
    plain.forEach(item => {
      if (item.parentId === parentId) {
        let child = {
          id: item.id,
          title: item.title,
          link: item.link,
          order: item.order,
          children: arraySortByOrder(getChildren(item.id)),
          attributes: [],
        }
        children.push(child)
      }
    })
    return children
  }

  let tree = {
    id: 'root',
    title: 'Tree root',
    link: '/',
    children: getChildren('root'),
    attributes: [],
    order: 0,
  }

  return tree
}
