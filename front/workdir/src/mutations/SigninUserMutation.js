import {
  commitMutation,
  graphql
} from 'react-relay'
//
import environment from '../Environment'


const mutation = graphql`
  mutation SigninUserMutation($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      user {
        id
        username
      }
    }

    tokenAuth(username: $username, password: $password) {
      token
    }
  }
`

export default (username, password, callback) => {
  const variables = {
    username,
    password,
    clientMutationId: '',
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        if (response.login) {
          const token = response.tokenAuth.token
          const username = response.login.user.username
          const id = response.login.user.id
          console.log('response token', token);
          callback(true, id, token, username)
        } else {
          console.log('response', response);
          callback(false, 0, '', '')
        }
      },
      onError: err => console.error(err),
    },
  )
}
