import {
  commitMutation,
  graphql
} from 'react-relay'
//
import environment from '../Environment'


const mutation = graphql`
  mutation CreateMergeMutation($input: CreateMergeInput!) {
    mergeProject(input: $input) {
      masterProject {
        id
        title
      }
    }
  }
`


export default (masterId, secondId, callback) => {
  const variables = {
    input: {
      masterId,
      secondId,
    },
    clientMutationId: '',
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        callback(response)
      },
      onError: err => console.error(err),
    },
  )
}
